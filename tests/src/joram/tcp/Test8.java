/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2019 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package joram.tcp;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.objectweb.joram.client.jms.Queue;
import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.tcp.TcpConnectionFactory;

import fr.dyade.aaa.agent.AgentServer;

/**
 * Test server configuration without a3servers.xml file.
 */
public class Test8 extends framework.TestCase {
  public static void main(String args[]) throws Exception {
    new Test8().run();
  }

  public void run() {
    try {
      AgentServer.setDefaultConfig(0, "localhost", "admin", "admin", 2560, 2570, null);
      AgentServer.init((short) 0, "s0", null);
      AgentServer.start();
      Thread.sleep(1000L);

      TcpConnectionFactory cf = TcpConnectionFactory.create("localhost", 2560);
      cf.getParameters().connectingTimer = Integer.getInteger("connectingTimer", 10);
      AdminModule.connect(cf, "admin", "admin");

      User.create("anonymous", "anonymous", 0);
      
      Queue queue = Queue.create(0);
      queue.setFreeReading();
      queue.setFreeWriting();
 
      AdminModule.disconnect();

      Connection cnx = cf.createConnection();

      Session sess1 = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageProducer producer = sess1.createProducer(null);

      Session sess2 = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageConsumer qconsumer = sess2.createConsumer(queue);

      cnx.start();

      TextMessage msg1 = sess1.createTextMessage();
      msg1.setText("Test Queue with LocalConnectionFactory");
      producer.send(queue, msg1);

      TextMessage msg2 = (TextMessage) qconsumer.receive(1000L);
      assertNotNull(msg2);
      if (msg2 != null) assertTrue(msg1.getText().equals(msg2.getText()));

      cnx.close();
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      System.out.println("AgentServer.stop()");
      AgentServer.stop();
      endTest();
    }
  }
}
