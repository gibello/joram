package joram.base;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TemporaryQueue;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.DeadMQueue;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

/**
 * Test the behavior of a TemporaryQueue on automatic deletion (server stop or kill) with
 * "org.objectweb.joram.tempQ.dmqOnDel" property not defined (default).
 * Remaining messages are deleted.
 */
public class Test_Temp_Q6 extends TestCase {
  public static void main(String[] args) {
    new Test_Temp_Q6().run();
  }

  DeadMQueue dmq = null;
  
  public void run() {
    try {
      System.out.println("server start");
      startAgentServer((short) 0);
      
      admin();
      System.out.println("admin config ok");

      Context  ictx = new InitialContext();
      ConnectionFactory cf = (ConnectionFactory) ictx.lookup("cf");
      ictx.close();

      Connection cnx1 = cf.createConnection();
      Session sessionp = cnx1.createSession(false, Session.AUTO_ACKNOWLEDGE);
      // temporary queue is creating by a session 
      TemporaryQueue tempqueue= sessionp.createTemporaryQueue();
      cnx1.start();
      MessageProducer producer = sessionp.createProducer(tempqueue);

      // create a message send to the queue by the pruducer 
      TextMessage msg = sessionp.createTextMessage();
      msg.setText("test_temporary_queue#1");
      producer.send(msg);
      msg = sessionp.createTextMessage();
      msg.setText("test_temporary_queue#1");
      producer.send(msg);
      
      // Closes the connexion so the temporay queue should be deleted.
      killAgentServer((short) 0);
      Thread.sleep(2000L);
      System.out.println("server restart");
      startAgentServer((short) 0);
      Thread.sleep(2000L);

      Connection cnx2 = cf.createConnection();
      Session sessionc = cnx2.createSession(false, Session.AUTO_ACKNOWLEDGE);
      cnx2.start();
      MessageConsumer consumer = sessionc.createConsumer(dmq);
      
      // the consumer try to receive the message from the DMQ
      TextMessage msg1 = (TextMessage) consumer.receive(5000L);
      System.out.println(msg1);
      assertTrue(msg1 == null);

      cnx2.close();
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    }
    finally {
      System.out.println("Server stop ");
      stopAgentServer((short)0);
      endTest(); 
    }
  }

  /**
   * Admin : Create a user anonymous
   *   use jndi
   */
  public void admin() throws Exception {
    AdminModule.connect("localhost", 2560, "root", "root", 60);
    User.create("anonymous", "anonymous");
    
    // create a deadqueue for receive expired messages
    dmq = (DeadMQueue) DeadMQueue.create();
    dmq.setFreeReading();
    dmq.setFreeWriting();
    AdminModule.setDefaultDMQ(dmq);

    ConnectionFactory cf = TcpConnectionFactory.create("localhost", 2560);

    Context jndiCtx = new InitialContext();
    jndiCtx.bind("cf", cf);
    jndiCtx.close();

    AdminModule.disconnect();
  }




}
