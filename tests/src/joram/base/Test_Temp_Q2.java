/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C)  2007 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s):Badolle Fabien (ScalAgent D.T.)
 * Contributor(s): 
 */
package joram.base;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TemporaryQueue;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.admin.DeadMQueue;
import org.objectweb.joram.client.jms.tcp.TcpConnectionFactory;

import framework.TestCase;

/**
 * Test the behavior of a TemporaryQueue on deletion with  property
 * "org.objectweb.joram.tempQ.dmqOnDel" not defined (default).
 * Remaining messages should be deleted.
 */
public class Test_Temp_Q2 extends TestCase {
  public static void main(String[] args) {
    new Test_Temp_Q2().run();
  }

  DeadMQueue dmq = null;
  
  public void run() {
    try {
      System.out.println("server start");
      startAgentServer((short)0);

      admin();
      System.out.println("admin config ok");

      Context  ictx = new InitialContext();
      ConnectionFactory cf = (ConnectionFactory) ictx.lookup("cf");
      ictx.close();

      Connection cnx = cf.createConnection();
      Session sessionp = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      Session sessionc = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      // temporary queue is creating by a session 
      TemporaryQueue tempqueue= sessionp.createTemporaryQueue();
      cnx.start();

      // create a producer and a consumer
      MessageProducer producer = sessionp.createProducer(tempqueue);
      MessageConsumer consumer = sessionc.createConsumer(dmq);
      // create a message send to the queue by the pruducer 
      TextMessage msg = sessionp.createTextMessage();
      msg.setText("test_temporary_queue#1");
      producer.send(msg);
      msg = sessionp.createTextMessage();
      msg.setText("test_temporary_queue#2");
      producer.send(msg);

      tempqueue.delete();

      // the consumer try to receive the message from the DMQ
      TextMessage msg1=(TextMessage) consumer.receive(5000L);
      assertTrue(msg1 == null);

      cnx.close();
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    }
    finally {
      System.out.println("Server stop ");
      stopAgentServer((short)0);
      endTest(); 
    }
  }

  /**
   * Admin : Create a user anonymous
   *   use jndi
   */
  public void admin() throws Exception {
    AdminModule.connect("localhost", 2560, "root", "root", 60);
    User.create("anonymous", "anonymous");
    
    // create a deadqueue for receive expired messages
    dmq = (DeadMQueue) DeadMQueue.create();
    dmq.setFreeReading();
    dmq.setFreeWriting();
    AdminModule.setDefaultDMQ(dmq);

    ConnectionFactory cf = TcpConnectionFactory.create("localhost", 2560);

    Context jndiCtx = new InitialContext();
    jndiCtx.bind("cf", cf);
    jndiCtx.close();

    AdminModule.disconnect();
  }
}

