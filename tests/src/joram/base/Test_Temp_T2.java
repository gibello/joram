/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C)  2007 - 2019 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package joram.base;

import java.util.List;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Session;
import javax.jms.TemporaryTopic;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.objectweb.joram.client.jms.admin.AdminModule;

import framework.TestCase;

import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.tcp.TcpConnectionFactory;

/**
 * Test the desletion of a Temporary Topic.
 * Verify that the remaining messages are not sent to the DMQ. 
 */
public class Test_Temp_T2 extends TestCase  {
  static Destination adminTopic = null;
  
  public static void main(String[] args) {
    new Test_Temp_T2().run();
  }

  public static void printDestinations(Destination[] destinations) {
    System.out.print("destinations=[");
    for (Destination destination : destinations) {
      System.out.print(destination + " ");
    }
    System.out.println("]");
  }
  
  public void run() {
    try {
      System.out.println("server start");
      startAgentServer((short)0);

      admin();
      System.out.println("admin config ok");

      Context  ictx = new InitialContext();
      ConnectionFactory cf = (ConnectionFactory) ictx.lookup("cf");
      ictx.close();

      org.objectweb.joram.client.jms.admin.AdminModule.connect("localhost", 2560, "root", "root", 60);

      Destination[] destinations = AdminModule.getDestinations();
      printDestinations(destinations);
      adminTopic = destinations[0];

      // temporary topic is creating by a session 
      Connection cnx = cf.createConnection();
      cnx.start();
      Session sessionp = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);   
      sessionp.createTemporaryTopic();
      cnx.close();
      
      Thread.sleep(1000L);
      testTempTopic();
      
      cnx = cf.createConnection();
      cnx.start();
      sessionp = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);   
      
      sessionp.createTemporaryTopic();
      sessionp.createTemporaryTopic();
      sessionp.createTemporaryTopic();
      sessionp.createTemporaryTopic();
      sessionp.createTemporaryTopic();
      cnx.close();
      
      Thread.sleep(1000L);
      testTempTopic();
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      System.out.println("Server stop ");
      stopAgentServer((short)0);
      endTest(); 
    }
  }

  /**
   * Admin : Create a user anonymous
   *   use jndi
   */
  public void admin() throws Exception {
    // Connexion to the Joram server
    AdminModule.connect("localhost", 2560, "root", "root", 60);
    // Creates a user
    User user = User.create("anonymous", "anonymous");
    // Initializes the ConnectionFactory
    javax.jms.ConnectionFactory cf = TcpConnectionFactory.create("localhost", 2560);

    javax.naming.Context jndiCtx = new javax.naming.InitialContext();
    jndiCtx.bind("cf", cf);
    jndiCtx.close();
    
    org.objectweb.joram.client.jms.admin.AdminModule.disconnect();
  }

  private static void  testTempTopic() throws Exception{
    Destination[] destinations = AdminModule.getDestinations();
    printDestinations(destinations);
    assertEquals(1, destinations.length);
    for (Destination destination : destinations) {
      assertEquals(adminTopic, destination);
    }
  }
}

