/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C)  2016 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s):
 * Contributor(s): 
 */
package jms.jms2.integrationtest;

import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.junit.jupiter.api.Test;
import org.objectweb.joram.client.jms.Queue;
import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.tcp.TcpConnectionFactory;
import org.ow2.joram.testutils.AgentManager;

import junit.framework.TestCase;

/**
 * Test : The message received by the consumer rollback, recover or throw an exception.
 * The delivery delay is set on server. The message must re-delivered
 * after the delivery delay.
 */
public class RedeliveryDelayQAdminTest extends TestCase implements javax.jms.MessageListener {

  JMSContext context;
  JMSConsumer consumer;

  ConnectionFactory cf = null;
  Queue dest = null;

  public void setUp() throws Exception {
    cf = TcpConnectionFactory.create("localhost", 16010);
    AdminModule.connect(cf, "root", "root");   
    User.create("anonymous", "anonymous", 0);
    // Use admin api
    System.out.println("Configure Queue redelivery delay.");
    dest = Queue.create(0, "RedeliveryDelayQAdminTest");
    dest.setRedeliveryDelay(5);

    dest.setFreeReading();
    dest.setFreeWriting();
    AdminModule.disconnect();
    
  }

  @Test
  public void testRedeliverySessionTransacted() throws Exception {
	  deliverMessage(JMSContext.SESSION_TRANSACTED);
	  AgentManager.restartAgentServer();
      System.out.println("Server restarted.");
      deliverMessage(JMSContext.SESSION_TRANSACTED);
  }
  
  @Test
  public void testRedeliveryClientAcknowledge() throws Exception {
	  deliverMessage(JMSContext.CLIENT_ACKNOWLEDGE);
	  AgentManager.restartAgentServer();
      System.out.println("Server restarted.");
      deliverMessage(JMSContext.CLIENT_ACKNOWLEDGE);
  }
  
  @Test
  public void testRedeliveryAutoAcknowledge() throws Exception {
	  deliverMessage(JMSContext.AUTO_ACKNOWLEDGE);
	  AgentManager.restartAgentServer();
      System.out.println("Server restarted.");
      deliverMessage(JMSContext.AUTO_ACKNOWLEDGE);
  }

  void deliverMessage(int sessionMode) throws InterruptedException {
    reset();
    switch (sessionMode) {
    case JMSContext.SESSION_TRANSACTED:
      context = cf.createContext(JMSContext.SESSION_TRANSACTED);
      break;
    case JMSContext.CLIENT_ACKNOWLEDGE:
      context = cf.createContext(JMSContext.CLIENT_ACKNOWLEDGE);
      break;
    default:
      context = cf.createContext(JMSContext.AUTO_ACKNOWLEDGE);
      break;
    }

    consumer = context.createConsumer(dest);
    consumer.setMessageListener(this);
    context.start();

    JMSContext prodCtx = cf.createContext();
    JMSProducer producer = prodCtx.createProducer();
    producer.setDeliveryMode(DeliveryMode.PERSISTENT);
    TextMessage msg = prodCtx.createTextMessage("test redeliveryTime");
    producer.send(dest, msg);

    // Wait to receive the message.
    Thread.sleep(10000L);

    assertTrue("The rollback or recover message not received after the redelivery delay", time >= 5000);

    context.close();
    prodCtx.close();
  }

  public void reset() {
    first = true;
    time = 0;
  }

  private boolean first = true;
  private long time = 0;

  public void onMessage(Message message) {
    try {
      System.out.println(message.getJMSMessageID() + ", JMSRedelivered = " + message.getJMSRedelivered());
      System.out.println(System.currentTimeMillis() + ": message received deliveryTime = " + message.getJMSDeliveryTime());
    } catch (JMSException e) {
      e.printStackTrace();
    }

    time = System.currentTimeMillis() - time;
    System.out.println("Waiting (" + first + ") " + time);

    if (first) {
      first = false;
      switch (context.getSessionMode()) {
      case JMSContext.SESSION_TRANSACTED:
        System.out.println("rollback");
        context.rollback();
        break;
      case JMSContext.CLIENT_ACKNOWLEDGE:
        System.out.println("recover");
        context.recover();
        break;
      default:
        System.out.println("throw RuntimeException");
        throw new RuntimeException("Test redeliveryTime");
      }
    } else {
      switch (context.getSessionMode()) {
      case JMSContext.SESSION_TRANSACTED:
        System.out.println("commit");
        context.commit();
        break;
      case JMSContext.CLIENT_ACKNOWLEDGE:
        System.out.println("acknowledge");
        context.acknowledge();
        break;
      default:
        System.out.println("nothing");
      }      
    }
  }
}
