/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2013 - 2019 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): 
 */
package jms.jms2;

import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.TextMessage;

import org.junit.jupiter.api.Test;
import org.objectweb.joram.client.jms.Topic;
import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.tcp.TcpConnectionFactory;

import junit.framework.TestCase;

/**
 * Test durable subscription.
 */
public class Jms9Test extends TestCase {

	@Test
	public void testJms() throws Exception {

		ConnectionFactory cf = TcpConnectionFactory.create("localhost", 16010);
		AdminModule.connect(cf, "root", "root");   
		User.create("anonymous", "anonymous", 0);
		Topic topic = Topic.create("Jms9Test");
		topic.setFreeReading();
		topic.setFreeWriting();
		AdminModule.disconnect();

		JMSContext context = cf.createContext();
		context.setClientID("MyClientID");
		JMSProducer producer = context.createProducer();
		JMSConsumer consumer = context.createDurableConsumer(topic, "dursub9", "lastMessage = TRUE", false);

		context.start();

		{
			TextMessage msg = context.createTextMessage("message");

			msg.setStringProperty("name", "first");
			msg.setBooleanProperty("lastMessage", false);
			producer.send(topic, msg);

			msg.setBooleanProperty("lastMessage", true);
			msg.setStringProperty("name", "second");
			producer.send(topic, msg);

			TextMessage recv = (TextMessage) consumer.receive();
			assertTrue("Receive bad message", (recv.getStringProperty("name").equals("second")));
		}

		context.close();
	}

}
