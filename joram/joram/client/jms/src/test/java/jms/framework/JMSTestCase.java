/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2002 - 2007 ScalAgent Distributed Technologies
 * Copyright (C) 2002 INRIA
 * Contact: joram-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): Jeff Mesnil (Inria)
 * Contributor(s): Nicolas Tachker (ScalAgent D.T.)
 */

package jms.framework;

import java.io.File;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.jms.JMSException;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.ow2.joram.testutils.SCAdminClassic;
import org.ow2.joram.testutils.SCAdminOSGi;

import junit.framework.TestCase;



/**
 * Class extending <code>framework.TestCase</code> to provide a new
 * <code>fail()</code> method with an <code>Exception</code> as parameter.
 * <br />
 * Every Test Case for JMS should extend this class instead of
 * <code>framework.TestCase</code>
 * 
 */
public abstract class JMSTestCase extends TestCase {
  
  /**
   * Fails a test with an exception which will be used for a message.
   * 
   * If the exception is an instance of <code>javax.jms.JMSException</code>,
   * the message of the failure will contained both the JMSException and its
   * linked exception (provided there's one).
   */
  public void fail(Exception e) {
    if (e instanceof javax.jms.JMSException) {
      JMSException exception = (JMSException) e;
      String message = e.toString();
      Exception linkedException = exception.getLinkedException();
      if (linkedException != null) {
        message += " [linked exception: " + linkedException + "]";
      }
      super.fail(message);
    } else {
      super.fail(e.getMessage());
    }
  }

  public JMSTestCase() {
    super();
    // Changes the class path with absolute paths
    try {
      Properties sysprops = System.getProperties();
      String classpath = sysprops.getProperty("java.class.path");
      classpath = getAbsolutePath(classpath);
      sysprops.setProperty("java.class.path", classpath);
    } catch (Exception exc) {
      throw new Error("cannot set absolute classpath");
    }
  }
  
/*
  @BeforeAll
  public static void startAgentServer() throws Exception {
	  SCAdminOSGi admin = new SCAdminOSGi();
	  //TODO select admin type ?? SCAdminClassic admin = new SCAdminClassic();
	  try {
		  admin.startAgentServer((short)0);
	  } catch (IllegalStateException exc) {
		  // The process is still alive, kill it!
		  admin.killAgentServer((short)0);
		  Thread.sleep(1000);
		  admin.startAgentServer((short)0);
		  throw(exc);
	  }
	  Thread.sleep(1000);
  }
  
  @AfterAll
  public static void stopAgentServer() {
	  SCAdminOSGi admin = new SCAdminOSGi();
	  try {
		  admin.stopAgentServer((short)0);
	  } catch (Exception exc) {
		  //ignore
	  }
	  //TODO endTest() to report test results ?? also see error(exception) in old BaseTestCase
	  //endTest(null, false);
  }
*/
  
  /**
   * Change the elements of a path as absolute names.
   * Use the property path.separator as separator.
   *
   * @param path	path to transform
   *
   * @return	transformed path
   */
  public static String getAbsolutePath(String path) throws Exception {
    String ps = System.getProperty("path.separator");
    StringTokenizer st = new StringTokenizer(path, ps);
    if (! st.hasMoreTokens())
      return path;

    StringBuffer buf = new StringBuffer();
    token_loop:
    while (true) {
      String tok = st.nextToken();
      buf.append(new File(tok).getAbsolutePath());
      if (! st.hasMoreTokens())
	break token_loop;
      buf.append(ps);
    }

    return buf.toString();
  }

  abstract public void setUp();
  
  abstract public void tearDown();
}
