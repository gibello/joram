package org.objectweb.joram.client.jms.admin;

import java.net.ConnectException;

import org.junit.jupiter.api.Test;

import jms.framework.PTPTestCase;

public class RestDistributionQueueTest extends PTPTestCase {

	@Test
	public void testCreate() {
		try {
			RestDistributionQueue queue = new RestDistributionQueue();
			assertNotNull(queue.create("RestDistributionQueue"));
			assertNotNull(queue.create(0, "RestDistributionQueue2"));
			
			assertEquals(queue.getHost(), "localhost");
			assertEquals(queue.getPort(), 8989);
			assertEquals(queue.getUserName(), "anonymous");
			assertEquals(queue.getPassword(), "anonymous");
			assertTrue(queue.isBatch());
			assertTrue(queue.isAsync());
			assertEquals(queue.getPeriod(), 1000);
			assertEquals(queue.getIdleTimeout(), 60);
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(AdminException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
