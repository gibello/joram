/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2002 - 2007 ScalAgent Distributed Technologies
 * Copyright (C) 2002 INRIA
 * Contact: joram-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): Jeff Mesnil (Inria)
 * Contributor(s): Nicolas Tachker (ScalAgent D.T.)
 */

package org.ow2.joram.testutils;

import java.io.File;
import java.util.Properties;
import java.util.StringTokenizer;

import org.ow2.joram.testutils.SCAdminOSGi;


/**
 * Start / Stop Joram agent server.
 * 
 */
public class AgentManager {

	public static SCAdminOSGi admin = new SCAdminOSGi();

	public AgentManager() {
		super();
		// Changes the class path with absolute paths
		try {
			Properties sysprops = System.getProperties();
			String classpath = sysprops.getProperty("java.class.path");
			classpath = getAbsolutePath(classpath);
			sysprops.setProperty("java.class.path", classpath);
		} catch (Exception exc) {
			throw new Error("cannot set absolute classpath");
		}
	}

	/**
	 * Start Joram agent server
	 * @throws Exception
	 */
	public static void startAgentServer() throws Exception {
		//SCAdminOSGi admin = new SCAdminOSGi();
		//TODO select admin type ?? SCAdminClassic admin = new SCAdminClassic();
		try {
			admin.startAgentServer((short)0);
		} catch (IllegalStateException exc) {
			// The process is still alive, kill it!
			admin.killAgentServer((short)0);
			Thread.sleep(1000);
			admin.startAgentServer((short)0);
			throw(exc);
		}
		//Thread.sleep(1000);
	}

	/**
	 * Stop Joram agent server
	 * @throws Exception
	 */
	public static void stopAgentServer() throws Exception {
		//SCAdminOSGi admin = new SCAdminOSGi();
		try {
			admin.stopAgentServer((short)0);
		} catch (Exception exc) {
			//ignore ?
			throw(exc);
		}
		//TODO endTest() to report test results ?? also see error(exception) in old BaseTestCase
		//endTest(null, false);
	}

	public static void restartAgentServer() throws Exception {
		//Thread.sleep(1000);
		AgentManager.stopAgentServer();
		Thread.sleep(500);
		AgentManager.startAgentServer();
	}

	/**
	 * Change the elements of a path as absolute names.
	 * Use the property path.separator as separator.
	 *
	 * @param path	path to transform
	 *
	 * @return	transformed path
	 */
	private static String getAbsolutePath(String path) throws Exception {
		String ps = System.getProperty("path.separator");
		StringTokenizer st = new StringTokenizer(path, ps);
		if (! st.hasMoreTokens())
			return path;

		StringBuffer buf = new StringBuffer();
		token_loop:
			while (true) {
				String tok = st.nextToken();
				buf.append(new File(tok).getAbsolutePath());
				if (! st.hasMoreTokens())
					break token_loop;
				buf.append(ps);
			}

		return buf.toString();
	}
	
	public static void main(String args[]) throws Exception {
		AgentManager.startAgentServer();
	}

}
