/*
 * Copyright (C) 2019 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 */
package fr.dyade.aaa.agent;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

/**
 * Object indicating that the configuration object has been updated in the transaction
 * base. This makes it possible to initialize the server in a slightly different way, for
 * example reading anew the services list.
 * 
 * Currently we simply consider the fact that the configuration has been modified, in
 * the future this object could contain indications on the changes to be made.
 */
public class ServerUpdate implements Serializable {
  /** Define serialVersionUID for interoperability. */
  private static final long serialVersionUID = 1L;
  
  /**
   * Date of the update.
   */
  private String update = null;
  
  /**
   * Creates a new empty ServerUpdate
   */
  public ServerUpdate() {
    update = new Date().toString();
  }
  
  private void writeObject(java.io.ObjectOutputStream out) throws IOException {
    out.writeUTF(update);
  }

  private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
    update = in.readUTF();
  }

  public String getUpdateDate() {
    return update;
  }
}
