/*
 * Copyright (C) 2006 - 2019 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package fr.dyade.aaa.util;

public interface DBTransactionMBean extends TransactionMBean {

  /**
   * Returns the pool size for <code>operation</code> objects, by default 1000.
   *
   * @return The pool size for <code>operation</code> objects.
   */
  int getLogThresholdOperation();

  /**
   *
   */
  int getPhase();
  String getPhaseInfo();
  
  String getDBTableName();
  
  String getDBInsertStatement();
  String getDBUpdateStatement();
  String getDBDeleteStatement();
  String getDBCloseStatement();

  /**
   * Returns the number of bytes written to database since last boot.
   *
   * @return The number of commit operation.
   */
  long getCommitBytes();
  
  /**
   * Returns the number of inserts in database since last boot.
   *
   * @return The number of inserts in database.
   */
  int getNbInserts();
  
  /**
   * Returns the number of bad inserts in database since last boot (record already existing).
   *
   * @return The number of bad insert in database.
   */
  int getBadInserts();
  
  /**
   * Returns the number of updates  in database since last boot.
   *
   * @return The number of updates in database.
   */
  int getNbUpdates();
  
  /**
   * Returns the number of bad updates in database since last boot (record needing to be inserted).
   *
   * @return The number of bad updates in database.
   */
  int getBadUpdates();
  
  /**
   * Returns the number of deletes in database since last boot.
   *
   * @return The number of deletes in database.
   */
  int getNbDeletes();

  String getObject(String dirname, String name);
  String getObjectList(String prefix);
}
